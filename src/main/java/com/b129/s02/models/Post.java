package com.b129.s02.models;


import javax.persistence.*;


@Entity
@Table(name = "posts")
public class Post {

    // Properties
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    //Constructors
    public Post(){

    }

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    //Setters and Getters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }


}
