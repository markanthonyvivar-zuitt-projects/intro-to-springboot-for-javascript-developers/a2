package com.b129.s02.models;

import javax.persistence.*;




    @Entity
    @Table(name = "users")
    public class User {

        // Properties


        @Column
        private String username;

        @Column
        private String password;

        //Constructors
        public User(){

        }

        public User(String username, String password) {
            this.username = username;
            this.password = password;
        }

        //Setters and Getters
        public String getUsername() {
            return username;
        }

        public void setUsername(String username){
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password){
            this.password = password;
        }
}
